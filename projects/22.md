# Presenting Images as Qubits in Quantum Computation

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/22)_

## Group Members:
- Başak Ekinci
- Bill Gonzalez
- Kamil Dyga

## Introduction and Motivation

One of the most interesting areas of research in Quantum Computing is in image classification.  In this regard, our team proposed to study image classification using quantum computing methods, specifically for machine learning purposes.  This entailed utilizing common machine learning methods to process common image sets, but further utilizing quantum computing methods for image processing at various stages.  Our initial work for the QJam2021 session dealt with using a Quantum Neural Network (QNN) to process a standard image data set.  The goal was to use quantum computing methods to train and then test a QNN in image classificaton tasks.  We utilized various examples available on the internet. Our approach for our final project was to study various image representation methods and attempt to implement at least one.  This included studying FRQI (Flexible Representation of Quantum Images) and NEQR (Novel Enhanced Quantum Representation).

Machine learning is one of the most popular fields of research in computer science and engineering.  Machine learning has become a critical technology in a variety of fields. As a result, we discussed our issue, which is one of the most important in the field of machine learning in terms of quantum programming. Using a quantum neural network, we attempted to categorize two images (cat and frog).A similar investigation was conducted using the mnist data set [5].

During QJam2021, we attempted to classify the CIFAR-10 data set using a Quantum Neural Network (QNN) method. Quantum neural networks are computational neural network models which are based on the principles of quantum mechanics.
By definition a quantum neural network (QNN) is a machine learning model that combines artificial neural networks and quantum information. The goal is to develop more efficient algorithms.  The hope is to overcome challenges related to inefficient training steps as the dimensions of our data becomes large.

Because the three-color channel was fixed in the CIFAR-10 data set, we encountered some challenges during our initial efforts and determined our accuracy value to be about 0.50.

## Deliverables

- Final Report describing our work on FRQI and NEQR
- Video presentation(s)
  - Description of project and presentation of report
  - Demonstration of working FRQI and NEQR code
- Working code containing project code, a readme file including execution hints and instructions

## Discussions

As part of our study, we decided to experiment with different quantum encoding algorithms in order to improve the accuracy value. We used the X gate to encode classical images into quantum data points in previous work (During QJam2021).
When we looked into the topic, we discovered that images can be presented as qubits in quantum calculations in a variety of ways. The Flexible Representation of Quantum Images (FRQI) [1,2,3], which uses the Fourier transform to portray images as qubits, and NEQR [4] (Novel Enhanced Quantum Representation) are two such encoding methods.

### FRQI
To improve classification accuracy, we first planned to incorporate the FRQI algorithm [1] into our code. 

We initially performed research by reading and implementing an FRQI circuit available thorugh the IBM Qiskit Textbook chapter titled "Quantum Image Processing - FRQI and NEQR Image Representations".  We utilized a workspace on the IBM Quantum Experience framework to implement and code for the encoding of a 2x2 sample image with pixels that were all black (theta=0), all white (theta=pi/2) and 50% intensity ratio (theta=pi/4).

We then attempted to convert this to a circuit using cirq in a Google Colab environment, but we were unable to port completely although we feel we can continue the conversion after the submission of the report.

### Alternate Encoding

However, we were unable to complete the task, therefore we attempted to encode the classical image into quantum data points utilizing the X, Y, and Z gates in combination, as opposed to our prior effort in QJam2021.

## Outcomes of the Project

First, we have applied X gates to transfer pixels with value 1 in our image to qubits.

![X-Gate Circuit](https://github.com/WebheadTech/QCourse511-1/blob/main/report_images/x-gate.png?raw=true "X-Gate Circuit")

And second, we have applied X,Y and Z gates together to transfer pixels with value 1 in our image to qubits.

![X-Y-Z-Gate Circuit](https://github.com/WebheadTech/QCourse511-1/blob/main/report_images/x-y-z-gates.png?raw=true "X-Y-Z-Gate Circuit")

In 4x4 pixel images, using three epochs and 32 batch-size parameters, the second technique produces slightly better results.  Accuracy results are shown in the plots below.

_We have applied X gates to transfer pixels with value 1 in our image to qubits._
![X-Gate Result Graph](https://github.com/WebheadTech/QCourse511-1/blob/main/report_images/x-gate_graph.png?raw=true "X-Gate Result Graph")


_We have applied X,Y and Z gates together to transfer pixels with value 1 in our image to qubits._
![X-Y-Z-Gate Result Graph](https://github.com/WebheadTech/QCourse511-1/blob/main/report_images/x-y-z-gates_graph.png?raw=true "X-Y-Z-Gate Result Graph")

## Project Presentation
The following video represents the entire scope of our project including research and results.  Click on the icon below to download the video.

[![Final Presentation](https://github.com/WebheadTech/QCourse511-1/blob/main/videos/Q511-1%20Final%20Video_thumb_small.PNG?raw=true)](https://drive.google.com/file/d/1KGfUB1OB3Q1BA5a7nPYd2KymNVlM_cYe/view?usp=sharing "Final Presentation")

## Code Files
We uploaded all of our code files to a central repository for review, sharing and version management.  Although you will find additional files in our repository, the main files are listed below.

- [X_Y_Zgate.ipynb](https://github.com/WebheadTech/QCourse511-1/blob/e8396eb5b292203669eda5d04541d31c3d947803/X_Y_Zgate.ipynb): File containing representation of images as qubits using X, Y and Z gates.
- [FRQI.ipynb](https://github.com/WebheadTech/QCourse511-1/blob/f94af797960123f1b29c3363ab815060e5fd1ba1/FRQI.ipynb): File that contains attempt to convert FRQI code from Qiskit to Cirq.

## Concluding Remarks

As part of our ongoing project after QJam2021 we attempted to improve classification accuracy.  We first planned to add FRQI or NRQI encoding algorithms to our code. However, we were not able to complete the porting of the Qiskit FQIR code to Cirq so we resorted to encoding classical images into quantum data points using the X,Y, and Z gates together, as opposed to our prior work at QJam2021, where we used the X gate only to encode a classical image into a quantum datapoint. And we've included a comparison of the two ways here. We get significantly better results when we combine the X,Y,Z gates, as seen in the figures.

### Future Pursuits
In the future, we intend to implement the FRQI approach to improve our outcomes by using FRQI to encode the images.  We plan to port the example Qiskit code to Cirq.

### Challenges
Many of the challenges we experienced involved the different platforms used to study and implement our work.  Our team used a combination of the IBM Quantum Experience and Google Collab.  We also initially tried using Anaconda and Jupyter notebooks, but that did not go well when attempting to create isolated environments.  We experienced the best outcomes using Google Collab as some of the tools we used were native to Google, e.g. TensorFlow and TensorFlow Quantum.  Lastly, conversion of code from Qiskit to Cirq proved difficult.

## Individual Contributions

**Başak Ekinci** used a quantum neural network to analyze Cifar data by reviewing the literature.  Başak also worked on encoding the image data using Z gates and then a combined Z, X and Y gate conversion method. Başak also  assisted in the information gathering and report writing.

Başak Ekinci Video (click icon to download video)

[![Başak Ekinci Video](https://github.com/WebheadTech/QCourse511-1/blob/main/videos/basak_thumb_small.png?raw=true)](https://github.com/WebheadTech/QCourse511-1/blob/main/videos/video1098232226.mp4?raw=true "Başak Ekinci Video")

**Bill Gonzalez** studied the FRQI and NEQR image encoding code, ran the available code for FRQI in the IBM Quantum Experience environment and attempted to port the FRQI code from the IBM Quantum Experience written in Qiskit to Cirq for Google Colab.  Bill also set up the GitHub accounts and assisted in the information gathering and report writing.

Bill Gonzalez Video (click icon to download video)

[![Bill Gonzalez Video](https://github.com/WebheadTech/QCourse511-1/blob/main/videos/gonzalez_thumb_small.PNG?raw=true)](https://github.com/WebheadTech/QCourse511-1/blob/main/videos/Gonzalez%20Video_low.mp4?raw=true "Bill Gonzalez Video")

**Kamil Dyga (late submission)** studied the script image encoding code, working with colleagues I had the opportunity to learn new things. I helped more experienced colleagues with minor issues.

Video: https://drive.google.com/file/d/177AKYYV3hG8NzeS5o-rEcZYMSSvy2AS1/view?usp=sharing

# References

[1]: IBM. Quantum Image Processing - FRQI and NEQR Image Representations. https://qiskit.org/textbook/ch-applications/image-processing-frqi-neqr.html

[2]: Le, P.Q., Dong, F. & Hirota, K. A flexible representation of quantum images for polynomial preparation, image compression, and processing operations. Quantum Inf Process 10, 63–84 (2011). https://doi.org/10.1007/s11128-010-0177-y

[3]: Iliyasu, A.M., Jiang, Z., Yan, F., Quantum Computation-Based Image Representation, Processing Operations and Their Applications. Entropy 16, 5290-5338 (2014). https://www.researchgate.net/publication/266855738_Quantum_Computation-Based_Image_Representation_Processing_Operations_and_Their_Applications

[4]: Zhang, Y., Lu, K., Gao, Y., NEQR: A novel enhanced quantum representation of digital images. Quantum Information Processing 12 (2013). https://www.researchgate.net/publication/257641933_NEQR_A_novel_enhanced_quantum_representation_of_digital_images

[5]: The TensorFlow Authors, MNIST classification (2020). https://colab.research.google.com/github/tensorflow/quantum/blob/master/docs/tutorials/mnist.ipynb#scrollTo=7HjBeQxmh_VQ

[6]: TensorFlow: Quantum: Quantum data. https://www.tensorflow.org/quantum/tutorials/quantum_data

[7]: TensorFlow: TensorFlow Core: Convolutional Neural Network (CNN).
https://www.tensorflow.org/tutorials/images/cnn

[8]: Anonymous Authors (under blind review), Image Compression and Classification using Qubits and Quantum Deep Learning. ICLR (2022). https://openreview.net/pdf?id=t1QXzSGwr9
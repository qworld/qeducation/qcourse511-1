# Quantum Music

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/56)_

Samuel Zahorec, Sairupa Thota

**Abstract**

In our Quantum music project, we have used random walk on a cube to make a quantum circuit using jupyter notebook and python language and then the outputs were converted to notes and rhythms by simply running the quantum circuit. The corresponding notes and rhythms were used to create midi tracks by converting the outcome to the musical notations, and now once we have our music notation, we can play our quantum computer music anywhere anytime. We have used "Quantum Computer Music: Foundations and Initial Experiments" as our base paper to build our project. Together we worked on almost every aspect of quantum computer music, we looked into how musicians started working with classical computers to produce music, and as the quantum computing field is still evolving, how researchers/musicians are working together to make quantum music, we produced music by simply using random walk on a cube, but we can also make music using complex circuits that will increase the randomness of our musical notes. Due to constrain in time, we were able to make quantum computer music using a circuit and random walk on a cube, our future goal for the project is to build a complex circuit that works in complex structures not only on the cube.

**Acknoledgements**

We would like to express our gratitude to all the individuals who have been organizing such knowledgeable courses, productive events through QWORLD. We have put a lot of effort into this project. However, completing it would not have been possible without the support and guidance of a number of individuals and we would like to extend our sincere thanks to all of them. We are highly indebted to ABUZER YAKARYILMAZ and OZLEM SALEHI for their constant guidance and supervision. We would like to express our gratitude to all the members of our team who have contributed to the success of our project together.

Team of 5:

Devanshu Garg, IIT Kharagpur

Sai Nandan, Electrical Engineering, Karunya University 

Sairupa Thota, Guru Nanak Institutions Technical Campus

Samuel Zahorec, Institute of Particle and Nuclear Physics, Charles University

Shreya Satsangi, Dayalbagh Educational Institute

**Introduction**

Our motivation for making quantum music is the word music itself, if we can have quantum
finance, quantum computing, quantum mechanics, then why not, quantum music? We also
get closer to quantum circuits while working on our project. Our goal was to produce quantum computer music by using random walks on a cube or quantum circuit to get the rhythms and notes to produce music. The outputs of the resultant quantum circuit were converted into pitches and rhythms by simply running the code in the jupyter notebook. For each list quantum circuit was iterated several times always beginning in the state where it was measured in the previous step. The output quantum states were encoded into a particular pitch and particular rhythmic beats where the numbers that are assigned correspond to a MIDI frequency which was then used in creating a MIDI file.

**List of deliverables**

Repository link : (https://github.com/wait-a-sec/Quantum-Music-using-Random-Walks)

Presentation Video: (https://youtu.be/bOz2IEGwupY)

PDF of our presenttion:Quantum_Computer_Music_-2.pdf

Quantum Music produced by us: (https://youtu.be/SJzoIo9nogo)

**Outcomes**

We successfully created a MIDI File and created a music track using random walk on cube (https://youtu.be/bOz2IEGwupY). We learned manipulation of qubits using different gates, Generation of music track by writing code in python using MIDI tools. This project fulfilled the gap between quantum computation and music, we used quantum computing to create music. During the implementation of this project we have learned many things current works ,on going and future researches etc.

**Discussion**

The importance of the project outcomes describes the applications of quantum computing ,where it's applications are diverse one among them is music composition. It helps music composers to produce their own music.

**Contributions**

We were team of 5 members who equally contributed to this project. We equally shared the work like gathering the information, implementation, management, documentation. Dev and samuel worked on main part of the project i.e, creating code, quantum circuit generating pitches and rhythms. Sai nandan, sairupa and shreya worked on second part creating MIDI file, project management, and documentation.

**Our project**

In our Quantum music project, we have used random walk on a cube to make a quantum circuit using jupyter notebook and python language and the outputs were converted to notes and rhythms by simply running the quantum circuit. The corresponding notes and rhythms were used to create midi tracks by converting the outcome to the musical notations, and now once we have our music notation, we can play our quantum computer music anywhere any time. Simple steps that can be used to replicate our project.

- Preparation of the quantum circuit [Fig.23].

![image](https://gitlab.com/qworld/qeducation/qcourse511-1/uploads/f32bf567b9f3652f89a348e6330ca81c/image.png)

- Creation of two lists, one for the pitch and the other one for the rhythm [Tab.4].

![image](https://gitlab.com/qworld/qeducation/qcourse511-1/uploads/7fbce01c67bcab590dfe8dd3f9647b74/image.png)

- For each list quantum circuit was iterated several times always beginning in the state where it was measured in the previous step.

- Conversion from binary output notes (https://www.dcode.fr/music-sheet).

- Creation of MIDI file.

**Conclusing remarks**

In our Quantum music project, we have used random walk on a cube to make a quantum circuit using jupyter notebook and python language and then the outputs were converted to notes and rhythms by simply running the quantum circuit. The corresponding notes and rhythms were used to create midi tracks by converting the outcome to the musical notations, and now once we have our music notation, we can play our quantum computer music anywhere anytime. We have used "Quantum Computer Music: Foundations and Initial Experiments" as our base paper to build our project. Together we worked on almost every aspect of quantum computer music, we look into how musicians started working with classical computers to produce music, and as the quantum computing field is still evolving, how researchers/musicians are working together to make quantum music, we produced music by simply using random walk on a cube, but we can also make music using complex circuits that will increase the randomness of our musical notes. Due to constrain in time, we were able to make quantum computer music using a circuit and random walk on a cube, our future goal for the project is to build a complex circuit that works in complex structures not only on the cube.

**Future works**

Technology has changed music forever and will continue to do so as long as it remains the part of a human life. The same way as pop music sounds have been altered by the noises that computers generate, giving rise to the “electronic” genre . Tech’s effect on music will reverberate through the industry, revolutionising the balance between artists and powerful music labels . This opens the door for more experimental music to reach the world. With the help of computers, social media and even AI almost anything is possible in tomorrow’s music scene. The impact of quantum computer will be revolutionary and it will change the way we produced music before.

A.I. is the ability of a computer to not only retain and regurgitate certain information, but to evolve, learn and create new ways of interpreting and reciting information. The best comparison for this technology is the brain – a mechanism that can not only learn about its environment, but can learn about itself, and in turn begins to learn an A.I. in new ways, it includes algorithms and also implements general reasoning and self-correction or adaption mechanisms. Although A.I. is still in its initial stages, once it becomes fully developed, there is no telling how capable a technology it will become. AI/ML will bring quantum music to the new height, and improve our way of understanding the music.

**Next Steps Of Our Project**

For the next part of the project we would like to implement complex random walk. The final aim would be to generate whole arrangement of an Orchestra by using different instruments. This would be possible by a website or a python library where music enthusiasts can play around with different arrangements. Using continuous variable quantum programming we would like to represent the string based instruments more accurately.

**Individual Contributions**

- Sairupa

I worked on creation of MIDI file and helped in over all management of project ,and documentation. After getting pitches and rhythms from the circuit the corresponding pitch is assigned to a particular MIDI frequency , and rhythms are assigned to a particular rhythmic beat. Then a python library that allows to write MIDI file using MIDI Util was installed (Fig.1).

![image](https://gitlab.com/qworld/qeducation/qcourse511-1/uploads/59b2a0056016d16fc5561463d0284cfc/image.png)

- Samuel

In the beginning of the project I was helping to establish our goals so that our project would really put out its blossoms and that in the end we would have some tangible results. Concerning the main part of the project I contributed to the overall implementation of the circuit mentioned in the general article into the python language (Fig.2). This circuit basically consisted of 5 qubits from which 2 were used as auxiliary qubits and other 3 qubits were in the end measured in order to obtain a binary set that would define each pitch or rhythm. At the beginning two auxiliary qubits were entangled through Hadamard and controlled NOT gates. Then various combinations of NOT, controlled NOT and Toffoli gates were applied on the other 3 qubits. A candidate for pitch or rhythm was chosen as the most probable from a given run, i.e. the one that was measured in the most out of 1024 shots. It was important to reassure that when running the circuit again in order to obtain new value of pitch or rhythm, it would begin with qubits in the state that was chosen as the candidate in the previous run. In order to obtain 30 pitches and rhythms, the circuit needed to be run 30 times separately for pitches and rhythms.

![image](https://gitlab.com/qworld/qeducation/qcourse511-1/uploads/a56bef4e1d12aae7d15ad95e99d6a350/image.png)

**Evaluation**

We successfully created a MIDI File and created a music track using random walk on cube (https://youtu.be/bOz2IEGwupY).

**Conclusion**

We succeeded in producing quantum music using random walk on a cube.

**References**

[1]Bernhardt, C. (2019). Quantum Computing for Everyone. The MIT Press. ISBN: 978- 0262039253.

[2]Griffiths, D. J. and Schroeter, D. F. (2018). Introduction to Quantum Mechanics. Cambridge University Press. ISBN: 9781107189638.

[3]Kendon, V. M. (2006). “A random walk approach to quantum algorithms”. Philosophical Transactions of the Royal Society 364:3407-3422.

[4]Miranda, E. R. (2020). “The Arrival of Quantum Computer Music”. The Riff. 13 May 2020. Online publication: https://medium.com/the-riff/the-arrival-of-quantum-computermusiced1ce51a8b8f (Accessed on 08 May 2021).

[5]Miranda, E. R. (2001). Composing Music with Computers. Elsevier Focal Press. ISBN: 9780240515670.

[6]Quantum Computer Music: Foundations and Initial Experiments REFERENCES Miranda & Basak 2021 https://arxiv.org/pdf/2110.12408.pdf

[7]https://magenta.tensorflow.org/

[8]https://cs224d.stanford.edu/reports/allenh.pdf

[9]https://www.datanami.com/2020/02/28/making-senseofsound- what does-machine-learning-mean-for-music/

[10]https://www.technologyreview.com/2015/04/15/168638/firstquantum-music-composition-unveiled/

[11]https://www.sciencedaily.com/releases/2019/06/190618174352.htm

[12]https://www.technologynetworks.com/informatics/news/hear- musicians-jam-with-a-quantum-computer-336979

[13]https://www.researchgate.net/publication/331062043_Application_of_Grover%27s_Algorithm_on_the_ibmqx4_Quantum_Computer_to_Rule-based_Algorithmic_Music_Composition

[14]https://soundcloud.com/guardianaustralia/first-ever-recordingof-computer-music

[15]https://medium.com/qiskit/presenting-the-1st-internationalsymposium-on-quantum-computing-and-musical-creativity87a3301b9726

[16]https://iccmr-quantum.github.io/

[17]https://github.com/JavaFXpert/quantum-music-playground

[18]https://github.com/alexfreed7/algorythms

[19]https://github.com/iccmr-quantum/pqca

[20]https://pypi.org/project/musical-scales/

[21]https://www.dcode.fr/music-sheet

**QJAM 2021**

We've successfully contributed to QUANTUM COMPUTER MUSIC as a part of QJAM 2021. We succeeded the JAM as we got certificate of excellence. You can refer to the below link regarding details of our project in QJAM 

https://gitlab.com/qworld/qjam/2021/-/issues/7
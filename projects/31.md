# Quantum Simulator with Mathlab

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/31)_

**MEMBER(S)**

Mahendra Gupta

**INTRODUCTION**

Quantum Simulation is not easy to understand and it is time consuming process.Here we developed a simulator to lessen the complexity of simulation.our simulator based on basic quantum principle and mathematics.

**CHALLENGES** 

It is difficult to design of simulator for QBIbits greater than 3.It is not easy to design a simulator which applied on quantum circuit which has large number of quantum gate application.code is become very large and complicated.

**Q_SIMULATOR**

In this project we have developed simulator which taken whole quantum circuit schema in form of MATRIX.the rows of matrix represent Qbits and the columns represent QGATE applied on corresponding Qbits. First row represents MSB bit and last belongs to LSB.First column represents GATE applied first on corresponding qbit and subsequent column represents gate applied subsequently .The all gate application converted into  matrix by using tensor product function kron and matrix matrix multiplication. all Qbits represent as column vector  initialized as ZERO state vector.simulation results shows in form of histogram.

**DELIVERABLES**

- We have design simulator for **any no. of Qbit**

- any no. of gate application

- Implements IDENTITY I GATE
 
- Implements X GATE
 
- Implements Z GATE
 
- Implements HADAMARD H GATE

- Results shows in pictorial form

**CODE**

[Input_circuit_application_method.docx](/uploads/4a4ba43b0167a8fe28c2b351caee430e/Input_circuit_application_method.docx)

https://github.com/mq00000/q_simulator

**OUTCOMES**

We successfully implements I,X,Z,H gate result shows in attach file. 

[QUANTUM_GATE__implemantation.docx](/uploads/3ea8566dd6236aa7f949be7559251531/QUANTUM_GATE__implemantation.docx)

**CONCLUSION**

the simulator is very easy and works accurately.the results are given in pictorial form so they are graspable.there is scope to upgrade simulator for other quantum gate operation.

During this project and the QCourse 511-1 we learn lot of quantum computing concepts and there implementation.

we are very thankful to whole QWorld team for giving this opportunity.we also thankful to fellow participants for making such learning atmosphere.
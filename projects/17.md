# Quantum simulator

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/17)_

## Introduction

JAVA implementation for simple quantum simulator. It requirements are specified in QWorld's QBronze Project- [Your Quantum Simulator](https://gitlab.com/qworld/bronze-qiskit/-/blob/master/projects/Project_Your_Quantum_Simulator.ipynb).   
The target outcome is to implement a simple Quantum Program through implementation of Quantum Gates from scratch. All numerical and algebraic methods have been implemented from scratch, such as matrix multiplications.   

**Student Name:** Haralds Egliens

**Discord Handle:** haraldsegliens#6039

**Gitlab username:** @haraldsegliens

### Midterm deliverables

JAVA class that implements QuantumProgram. QuantumProgram supports only Hadamard, NOT, Z-gate and Rotation gates.

JAVA test class that tests the implemented QuantumProgram class.

### Final deliverables

JAVA class that implements QuantumProgram. QuantumProgram supports Hadamard, NOT, Z-gate, Rotation gates, CNOT and Controlled-Z gate.

JAVA test class that tests the implemented QuantumProgram class.

[Link to repository](https://gitlab.com/haraldsegliens/quantum-simulator)

## Outcomes

Project resulted in 5 JAVA classes:

- QuantumSystem.java - main outcome of the project - class that describes a quantum system. This class allows user to simulate quantum system by applying gates to it, measuring it. The class also has debugging features such as returning unitary matrix, return state.
- Gates.java - predefined gates that can be used by the QuantumSystem.java
- Matrix.java - JAVA implementation of Matrix that is used by QuantumSystem.java
- RandomCollection.java - weighted random choice collection that is used by QuantumSystem.java to choose a state based on multiple probabilities
- QuantumSimulatorTest.java - example command line JAVA runner class that demonstrates usage of QuantumSystem.java

### QuantumSimulatorTest.java output

```
Quantum circuit with H gate.
Observed probabilities are:
	00: 50,00%
	01: 50,00%
	10: 0,00%
	11: 0,00%
Current state of system:
	00: 0,71
	01: 0,71
	10: 0,00
	11: 0,00
Unitary matrix:
	0,71   0,71   0,00   0,00   
	0,71   -0,71  0,00   0,00   
	0,00   0,00   0,71   0,71   
	0,00   0,00   0,71   -0,71  
Executed the quantum system for 1000 times
00 : 517
01 : 483



Quantum circuit with X gate.
Observed probabilities are:
	00: 0,00%
	01: 100,00%
	10: 0,00%
	11: 0,00%
Current state of system:
	00: 0,00
	01: 1,00
	10: 0,00
	11: 0,00
Unitary matrix:
	0,00   1,00   0,00   0,00   
	1,00   0,00   0,00   0,00   
	0,00   0,00   0,00   1,00   
	0,00   0,00   1,00   0,00   
Executed the quantum system for 1000 times
01 : 1000



Quantum circuit with Z gate.
Observed probabilities are:
	00: 100,00%
	01: 0,00%
	10: 0,00%
	11: 0,00%
Current state of system:
	00: 1,00
	01: 0,00
	10: 0,00
	11: 0,00
Unitary matrix:
	1,00   0,00   0,00   0,00   
	0,00   -1,00  0,00   0,00   
	0,00   0,00   1,00   0,00   
	0,00   0,00   0,00   -1,00  
Executed the quantum system for 1000 times
00 : 1000



Quantum circuit with ROTATION gate.
Observed probabilities are:
	0: 75,00%
	1: 25,00%
Current state of system:
	0: 0,87
	1: -0,50
Unitary matrix:
	0,87   -0,50  
	0,50   0,87   
Executed the quantum system for 1000 times
0 : 763
1 : 237



Quantum circuit with CNOT gate.
Observed probabilities are:
	00: 100,00%
	01: 0,00%
	10: 0,00%
	11: 0,00%
Current state of system:
	00: 1,00
	01: 0,00
	10: 0,00
	11: 0,00
Unitary matrix:
	1,00   0,00   0,00   0,00   
	0,00   1,00   0,00   0,00   
	0,00   0,00   0,00   1,00   
	0,00   0,00   1,00   0,00   
Executed the quantum system for 1000 times
00 : 1000



Quantum circuit with CZ gate.
Observed probabilities are:
	00: 100,00%
	01: 0,00%
	10: 0,00%
	11: 0,00%
Current state of system:
	00: 1,00
	01: 0,00
	10: 0,00
	11: 0,00
Unitary matrix:
	1,00   0,00   0,00   0,00   
	0,00   1,00   0,00   0,00   
	0,00   0,00   1,00   0,00   
	0,00   0,00   0,00   -1,00  
Executed the quantum system for 1000 times
00 : 1000



Quantum circuit with phase kickback.
Observed probabilities are:
	00: 0,00%
	01: 0,00%
	10: 0,00%
	11: 100,00%
Current state of system:
	00: 0,00
	01: 0,00
	10: 0,00
	11: 1,00
Unitary matrix:
	0,00   0,00   0,00   1,00   
	1,00   0,00   0,00   0,00   
	0,00   1,00   0,00   0,00   
	0,00   0,00   1,00   0,00   
Executed the quantum system for 1000 times
11 : 1000
```
 
## Discussions

Successfully achieved the project result by implementing a quantum simulator in JAVA. Project didn't use any external library such as apache common matrix operations.
Even though the quantum simulator is simple, it still required a lot of code and effort to make it work and readable by other programmers.


## References

[Project requirements](https://gitlab.com/qworld/bronze-qiskit/-/blob/master/projects/Project_Your_Quantum_Simulator.ipynb)
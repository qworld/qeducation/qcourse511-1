# Interactive visualization of single qubit noisy channels

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/26)_

The final project submission has been created in the [repository](https://github.com/korsakjakub/qcourse-channel-visualization)

# Introduction and motivation

Single qubit quantum channels can be represented as affine transformations of Bloch vectors (see Mike&Ike ch. 8). I would like to provide a visual and interactive way to play with some models of noise and see how they affect the space of input states.
The noisy channels should be easily implementable and composable

![Zrzut_ekranu_2021-12-8_o_11.53.43](https://gitlab.com/qworld/qeducation/qcourse511-1/uploads/23dbe5bea17a668b2a86b3a9914baa21/Zrzut_ekranu_2021-12-8_o_11.53.43.png)

# Member(s):
The project has been conducted by Jakub Korsak. I am studying Theoretical Physics at the Faculty of Physics, University of Warsaw. I am also a student at the Center for Theoretical Physics, Polish Academy of Sciences, where I learn and try to research the theory of Quantum Computing. 

# In the project there are the following deliverables:

## From midterm:

Created a function for the generation of sets of input states with given:

* array of state vectors
* array of density operators
* array of bloch vectors
* (with no input)

For the first 3 options, the resulting bloch vectors will be obtained from input, in the last one the vectors will be generated randomly (sample size can be adjusted with a variable).

Created a function returning the affine transformation (M, c)

* r' = M r + c for chosen noisy channel model. Available models are:
* Depolarizing
* Pauli (X, Y, Z)
* (Amplitude damping is in progress)

## In the final version:

* The noise level in the models is parametrized
* You can create noisy channels either by using the predefined ones (depolarizing and Pauli noise) or by writing it in form of the (M, c) affine transformation discussed above.
* The channels can be easily chained with matrix multiplication, and I showed how to easily apply them to the input Bloch vectors
* You can run a 3D interactive plot of the Bloch ball with the states put through noisy channels and change the amount of noise with a slider
* In the project notebook there is a simple demo in which you can create such figures:
![Figure: Project demo](https://raw.githubusercontent.com/korsakjakub/qcourse-channel-visualization/main/project-demo.png "a title")

# Closing remarks

It is a very simple project, but I think it is both well structured and general enough to be easily extended into more robust tools.
It can be used for visually understanding different models of noise, but also generating high quality figures for related projects.
During the work I encountered numerous programming problems, most of which (all of them) were my fault. I learned a lot about the tools needed for interactive visualization of quantum objects.

[Link to presentation](https://drive.google.com/file/d/1J6DsZyWn6BL8mS-HCSUtrFnJbV9g-V-6/view?usp=sharing)
# Musical Composition with Quantum Computing

_[click for the issue dedicated to this project](https://gitlab.com/qworld/qeducation/qcourse511-1/-/issues/51)_

Hazal Nisa Ensarioğlu, Can Siret Dosdoğru

### Introduction

Concept of the algorithmic composition is as old as the concept of music. Algorithmic composition started with some pre-determined rules and paper-pencil calculations, evolved with the classical computers and computer made calculations, took a new turn with the artificial intelligence and now, a whole new field is arising with rise of the quantum computers and algorithms; quantum algorithmic composition.  
Motivations for this project was to:
1. Prepare a crash course notebook summarizing the inspirations and history of the algorithmic composition, especially quantum algorithmic composition, describe some commonly used algorithms.
2. Put together our own original algorithm in that issue and implement it.
We prepared a mini-notebook for the first part of our project with encapsulated informations about algorithmic composition.
Then we created our own quantum algorithm to experiment with the idea.
We took the concept of the circle of fifth from the music theory, which means every member of the circle has a clockwise neighbor, being its perfect fifth. Perfect fifth corresponds to the next 7th note in the piano.  


### Outcomes

We talked about most of the classical ways and some of quantum counterparts of composing music by algorithms. Since it is a freshly blooming subject, there was not many well-defined algorithms and most of the research were theoretical. So we tried to tinge some fresh -although unripe- ideas into the pool of this exciting field.

We tried doing it so with implementing the unit circle to the circle of fifths in music theory and try to use it with the rotation and reflection operators to get relations of the notes in the circle of fifths.

The rotational operator implementation was a bit of success, but it could be done better. We only did a way to get different key changes every time we run the code and it brings us close changes because the closer the key changes, the easier.

The reflection operator was not so successful. We wanted to get the tritones of the notes , which mean the notes that are on the opposite side of the circle, but we could not figure out a way to alter the "reflection mirror" for every possible note. Also, in our example, the y=x reflection mirror does not seem to fit since the angle of that mirror wont fit with the angles of our notes.


### Discussions

Although out methods were not so great in this project time, these can be good ways of implementing this circle of fifths to the unit circle. A lot more change has to be done, but we think this is an innovative way of doing so.
For example, in our rotational operator part, the code can give 3 or more random outputs and line them up for the user to instantly create a key sequence. Furthermore, the starting can be any note, instead of us doing it only for the note C.

### Conclusing Remarks

It was hard to find projects in an intermediate way and implementing on Jupyter Notebook. We tried using information that was used in the lectures of this course and implement it in this project.

The research of the ways of composing music was entertaining and educational. It was fun to learn those methods all the way from experiments that was done B.C. and experiments that has been done recently. It is also nice to see that there is more to come to this subject.

The Jupyter Notebooks of the class helped us a lot to overcome the challenges. Implementing the circle of fifths into the unit circle was tricky at first. Creating the operators was also a bit challenging but we managed to overcome them. Although we couldn't do it as much as we planned, it's nice to see the way of doing it and hoping to improve our codes.

### Future Works
-We would like to improve our Circle of Fifths code as we discussed in the "Discussions" section, and maybe implement some other music theory concepts to quantum algorithms.
-We had some other exciting ideas we discussed but could not work on it because of the time interval and other college workloads, we would like to explore them as well. Such as:
-We will try to implement the Quantum Teleportation Algorithm to make compositions by teleport a musical note by teleporting its frequency and pitch data, then will substract melodies by in-process vectors and incorrectly teleported datas, since there is an experimental error rate. 
-We will try to make a "Quantum Guitar" simulation by converting classical strings (classical harmonic oscillator) to quantum strings (quantum harmonic oscillator)   

### Contributions:
- Can Siret Dosdoğru (Yıldız Technical University, Physics BA) : Implemented most of the operators on the circle of fifths and wrote the Quantum Era on the Jupyter Notebook.
- Hazal Nisa Ensarioğlu (Yıldız Technical University, Physics BA) : Wrote the classical computing part of the project and wrote the final deliverables part in the project.

--

Complete File: [Musical-Composing.ipynb](/uploads/f8af08b492dac2407735dff225203177/Musical-Composing.ipynb)

Presentation: https://www.youtube.com/watch?v=JxnxCYyKJLA&feature=youtu.be

https://streamable.com/qkcnnj

https://github.com/hazalens/Final-Deliverables.git

## References:

- Jupyter Notebooks of the lectures, mostly Week 3 Notebooks

- https://www.cs.cmu.edu/~music/cmsip/slides/05-algo-comp.pdf

- https://physics.wustl.edu/events/quantum-science-colloquium-what-quantum-markov-chain-beginner%E2%80%99s-look-non-commutative

- https://ccrma.stanford.edu/~blackrse/algorithm.html#:~:text=Besides%20the%20three%20various%20methods,different%20algorithmic%20composition%20systems%20produce.

- https://www.britannica.com/technology/Analytical-Engine

- https://everything.explained.today/Musica_universalis/

- https://jythonmusic.me/ch-6-randomness-and-choices/
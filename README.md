<img src="https://qworld.net/wp-content/uploads/2021/10/QCourse511_www.jpg">

# QCourse511-1

This repository is dedicated to the term projects of [QCourse511-1](https://qworld.net/qcourse511-1/).

For each project, an issue is created on this repo by following [our template](.gitlab/issue_templates/submission-template.md).  
We use commenting tools for the updates and communications.  
We use [Markdown](https://www.markdownguide.org/basic-syntax/) for formating the text.

Please read the whole document before asking any questions.

For your questions, please can use the Discord server of QCourse511-1.

## Grading

Each project is graded out of 30 points:

- The project outcomes and success of deliverables is 20 points.
- The project report is 5 points
- Presentation is 5 points.

The evaluation criteria is described later.

## Deadlines (AoE)

The deadlines are firm.

| Date   |  Task | if missed? |
| ------ | ------- | ---------- |
| Wed, Dec 8 | Submission of project proposals | -3 points |
| Sat, Dec 11 | Late submission of project proposals | -30 points |
| Sun, Dec 12 | Revision of proposals if needed |  |
| Sun, Dec 26 | Submission of midterm deliverables | -3 points |
| **Wed, Jan 12** | Submission of project reports and presentations | -3 points |
| Sat, Jan 15 | Late submission of final reports and presentations | -30 points |

As a part of the evaluation, verbal exams may be scheduled.

## Generic information and rules

The full name of each participant must be provided in the project documents.

### Project repo

Each project should have its public-access repo (Github, Gitlab, or similar).
  
The project developed for (or potentially developed for) QWorld may use a branch of this repo.  
_Specify this detail (if applicable) in the project proposal._  

Contact us if you think the nature of your project needs private access.  
_In this case, QCourse511 team should access the repo._


### Licensing of the projects

The project details and outcomes developed during QCourse511-1 should be publicly available, and they should have open-source licenses such as 
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) or 
[MIT license](https://opensource.org/licenses/MIT) or 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) or 
[Educational Community License](https://opensource.org/licenses/ECL-2.0).

After QCourse511-1, the licensing of the new features or developments are up to the project member(s).

Contact us if you think the nature of your project needs some other types of licensing. We may consider exceptions after evaluating the request within our team.

## Submission of project proposals

Each project proposal is submitted either by an individual or by a group of students.  
In the latter case, a single submission should be made by listing each project member explicitly.

Depending on the nature project, the proposal by a group may be asked to go with individual projects.

The submission of a project is made by creating an issue on this repo.  
When creating an issue, please use our template.  
The project team checks and verifies the updates.

Do not make any further updates after the deadline until the team confirms the submission.

### The structure of an issue

Any proposal must be submitted by Dec 8 with the following details:
- Project title
- Keywords
- Member(s)
- Project description
- Target outcome
- Midterm deliverables
- Final  deliverables

## Status 

We use several keywords to trace the status of projects:
- Proposal: late (-3 points)
- Proposal: revise
- In-Progress
- Midterm: completed
- Midterm: no-report (-3 points)
- Midterm: incomplete-report (-3 points)
- Final: submitted 
- Final: late (-3 points)
- Incomplete (-30 points)

These keywords are assigned by our team.
 

## Midterm deliverables

The midterm report is submitted as a comment of the issue before the deadline.

The details of the report should be verifiable. Otherwise, the report may be evaluated as incomplete.

_There is no late submission option for midterm reports._

## Evaluation criteria, project report, and presentation

The project (final) report is the main document where the evaluation starts.  
**If there is no project report, then the project is evaluated as 0.**  
**Only the mentioned and verifiable claims from the report will be evaluated.**

The project report must be submitted as a single comment by using Markdown (no other format is allowed).  
- _Do not place the project report outside the issue._  
- _We plan to add the reports of the successful projects to the main repository._  
- _Do not make any modification on the project report after the deadline.[*] Otherwise, it is considered as a late-submission._  

The links to the project repository and presentation(s) must be given in the project report.

It is the responsibles of students to make the project repositories easy to follow.

_[*] Remark that the timestamps are visible and traceable on Gitlab._

### Evaluation criteria for the final report
	
The project report is graded out of 5:
- 5: complete
- 4: good report with a few missing points
- 3: satisfactory, i.e., having basics but without good explanations
- 2: incomplete but still understandable
- 1: poor report

Penalties (up to -5) may be applied if
- the report is not readable or not understandable, or,
- the report has unverifiable claims, expecially about the outcomes of project[*].

The bonus (up to 2) may be applied if
- the report is well-structured, or, it has certain merits.

_[*] For example, there is no implementation code in the project repository even though the report claims that Shor's algorithm is implemented in Qiskit for N=93._

### Structure of the project report

Remark that:
- The final report is for understanding the project, outcomes, implementation details, and their importance, etc.  
- Keep the report simple as much as possible.  
- The final report is expected to be mostly text-based. Summary figures/tables can be added for sure.
- Use the presentation(s) for the visual elements or demo or test results, etc.

The overall structure of the project report is up to you. We provide a list here about the general expectations (not all of them is appliable for each project).
- Introduce your project: introduction and motivation, team member(s), available work/literature and your proposal, the list of deliveriables, etc.
- Outcomes of the project and their explanations. 
- Discussions: Success of the implementations, the importance of the outcomes, the contibution of the project, originality of work, etc.
- Conclusing remarks: summary of project, challenges during the implementations and how to overcome them, whether the outcomes can be extended and possible future work, etc.
- Individual contributions for the group projects.
- References

_If the project is linked to a QJam2021 project, the details must be added to the project report. If the details are missing, you may not recieve any bonus points due to QJam2021._

### Evaluation criteria for the presentation


The presentation is complementary to the final report, and it must be up to 10 minutes video recording. _Exceeding parts are not taken into account for evaluation._ 

If it is a group project, then each member must prepare a separate video recording up to 3 minutes for explaining her contributions.  

There is no specific format for the presentation.  
- You can verbally and visually explain your project by focusing on the outcomes and implementation details.
- For example, you can include a demo if the outcome is a software, or, you can show and explain the notebooks if it is a tutorial, etc.
- Do not read the final report as the video presentation.
- _You may use tools such as Zoom to make single session record or ActivePresenter to record part by part._ 
- It is up to you where to upload the recording(s).



The video presentation is graded out of 5:
- 7: Exceptional (+2 bonus)
- 6: Excellent (+1 bonus)
- 5: Very good
- 4: Good
- 3: Satisfactory
- 2: Poor
- 1: Incomplete

Penalty (up to -5) may be applied if the presentation has unverifiable claims, expecially about the outcomes of project.

For group projects, the individual evaluation is made by checking both group's recording and the person's recording. 

### Evaluation criteria for the project

After reading the project report and watching the presentation(s), the verifications are made on the project repo.

The outcomes and implementation are graded out of 20:
- 10 points: how successful the project is (whether enough deliverables for a month have been completed)
	- 6 is for satisfactory and 10 is for very good
- 10 points: how good the outcomes are
	- 7 is for satisfactory and 10 is for good
- 4 bonus points: originality/excellence of implementation or outcomes

## Project ideas

You have one month to complete your project, and you can always continue your project after QCourse.  
Therefore, adjust your expectations and deliverables accordingly.

### Do not

If you have some attractive ideas but do not know what to do, then do not pick them for your term project.

If you want to learn some topics as a part of the project but do not have any pre-knowledge, then do not pick them for your term project.

### Do

If you have some pre-knowledge on a topic but do not know what to do, then 
- preparing an introductory tutorial with similar format of Bronze or Silver or
- creating a quantum game about it 
may be a good strategy.

### Some ideas from QJam2021

You can check [the project ideas prepared for QJam2021](https://gitlab.com/qworld/qjam/2021/-/blob/main/Project_Ideas.md).

You can check [the projects proposed during QJam2021](https://gitlab.com/qworld/qjam/2021/-/issues).

You may check other quantum jams-hackathons or quantum challenges.

You may check the online talks during QCourse511.

### Bottom line

If you do not have any idea, you can implement the project "Your Quantum Simulator" given in our tutorial Bronze-Qiskit.

You can use any programming language that you are comfortable with.
You may revise the project with new ideas.

## Weekly meetings

After recieving project proposals, we will schedule weekly online meetings with the team to answer your questions.

The details will be announced on Discord.
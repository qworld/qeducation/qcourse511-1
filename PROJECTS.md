# The list of the submitted term projects

| # | Project Name | Student(s) |
| - | - | - |
| 21 | [Benchmarking Error Mitigation techniques](projects/21.md) | Amir Ebrahimi, Manvi Gusain, Ranendu Adhikary, Saumya Prakash Sharma, Tugba Özbilgin |
| 30 | [Introduction to Open Quantum Systems](projects/30.md) | Alvaro Rafael Gomez |
| 46 | [Bloch duel v2](projects/46.md) | Przemysław Michałowski, Krzysztof Królewicz, Vismaya Sunil |
| 4 | [HHL Algorithm and Quantum Linear Regression](projects/04.md) | Muhammad Jawad |
| 9 | [Introduction to Quantum Game Theory](projects/09.md) | Aman Shaikh |
| 33 | [Quantum automaton implementation](projects/33.md) | Aliia Khadieva |
| 13 | [A Mathematical Introduction to Quantum Computing](projects/13.md) | Israel Gelover |
| 22 | [Presenting Images as Qubits in Quantum Computation](projects/22.md) | Başak Ekinci, Bill Gonzalez, Kamil Dyga |
| 36 | [Introduction of Quantum walk: its application on search and decision-making](projects/36.md) | Hossein Khabazipour, Mohsen Izadyari, Hayede Zarei, Mostafa Shabani |
| 56 | [Quantum Music](projects/56.md) | Samuel Zahorec, Sairupa Thota |
| 5 | [Introduction to Quantum Chemistry](projects/05.md) | Malihe Yadavar, Vardaan Sahgal, Ajay Kumar, Asif Saad, Buvan Prajwal, Fazal Abbas |
| 26 | [Interactive visualization of single qubit noisy channels](projects/26.md) | Jakub Korsak |
| 12 | [Introduction to Quantum Machine Learning](projects/12.md) | Kumar Satyam, Lalit Kumar, Vikrant Kumar, Rupayan Bhattacharjee, Niaz Al Murshed |
| 19 | [Simulation of cryptanalysis and improvement of a quantum secure direct communication protocol with single photons](projects/19.md) | Ali Amerimehr |
| 18 | [Binary Digit Classifier Using QNN with GUI input](projects/18.md) | Sanidhya Gupta |
| 51 | [Musical Composition with Quantum Computing](projects/51.md) | Hazal Nisa Ensarioğlu, Can Siret Dosdoğru |
| 20 | [Basics of MindQuantum](projects/20.md) | Marija Šćekić |
| 6 | [Introduction to Quantum Error Correction](projects/06.md) | Harsh Gupta, Harpreet Singh Wazir |
| 8 | [Quantum ML Classifier](projects/08.md) | Amir Khan |
| 57 | [Route Optimization using Quantum Annealing](projects/57.md) | Munawar Ali |
| 14 | [Implementing Quantum Technologies in Medical Sciences](projects/14.md) | Simranjeet Singh Dahia |
| 54 | [Quantum relay system](projects/54.md) | Matias Mustonen |
| 15 | [Introduction and Implementation of Quantum Walks](projects/15.md) | Muhammad Hassaan Ghazali, Sadaf Aamer |
| 45 | [Quantum Merge: Where Physics & Computer Science Meet](projects/45.md) | Osama Arif Lone, Muhammad Usaid |
| 2| [Quantum Key Distribitution](projects/02.md) | Umair Ahmed |


### Quantum Simulators

| # | Project Name | Student |
| - | - | - |
| 24 | [QCSimulator: Simulating a Quantum Computer using C++](projects/24.md) | Meghana Ayyala Somayajula |
| 44 | [Quantum Simulator using Python](projects/44.md) | Shivalee RK Shah |
| 23 | [Quantum Simulator with Julia](projects/23.md) | Ricardo Enrique Bueno Knoop |
| 17 | [Quantum simulator with Java](projects/17.md) | Haralds Egliens |
| 52 | [Applying quantum concepts of the course in R](projects/52.md) | Carla Bailon Rosas |
| 39 | [Basic Quantum simulator in kotlin/java](projects/39.md) | Kapil Kodwani |
| 27 | [Quantum Circuit Simulator with C#](projects/27.md) | Muhammad Ali Alam |
| 31 | [Quantum Simulator with Matlab](projects/31.md) | Mahendra Gupta |